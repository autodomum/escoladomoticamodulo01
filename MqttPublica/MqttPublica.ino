#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include "DHT.h"

// CONSt WIFI
const char* SSID = "AGUIAR_ADM"; // rede wifi
const char* PASSWORD = "a0b9c8d7e6"; // senha da rede wifi

const char* BROKER_MQTT = "192.168.10.101"; // ip/host do broker
int BROKER_PORT = 1883; // porta do broker
const char* NAMECLIENTE = "ALUNO01";
const char* MQTTUSERNAME = "autohome";
const char* MQTTPASSWORD = "comida05";

const char* topicoPubTemp = "/casa/temp/01";
const char* topicoPubUmid = "/casa/umid/01";

#define DHTPIN D4 // pino que estamos conectado
#define DHTTYPE DHT11 // DHT 11

// prototypes
void initPins();
void initSerial();
void initWiFi();
void initMQTT();
void initDHT();
void pubDHT();

// Conecte pino 1 do sensor (esquerda) ao +5V
// Conecte pino 2 do sensor ao pino de dados definido em seu Arduino
// Conecte pino 4 do sensor ao GND
// Conecte o resistor de 10K entre pin 2 (dados) 
// e ao pino 1 (VCC) do sensor
DHT dht(DHTPIN, DHTTYPE);

WiFiClient espClient;
PubSubClient MQTT(espClient); // instancia o mqtt

// setup
void setup() {
//  initPins();
  initSerial();
  initWiFi();
  initMQTT();
  initDHT();
  pubDHT();
}

void loop() {
  if (!MQTT.connected()) {
    reconnectMQTT();
  }
  recconectWiFi();
  MQTT.loop();
  pubDHT();
}

// implementacao dos prototypes


void initDHT()
{
  Serial.println("Iniciar DHT11");
  dht.begin();
  
}

void pubDHT()
{
    // A leitura da temperatura e umidade pode levar 250ms!
  // O atraso do sensor pode chegar a 2 segundos.
  float h = dht.readHumidity();
  float t = dht.readTemperature();
  // testa se retorno é valido, caso contrário algo está errado.
  if (isnan(t) || isnan(h)) 
  {
    Serial.println("Failed to read from DHT");
  } 
  else
  {
    Serial.print("Umidade: ");
    Serial.print(h);
    Serial.print(" %t");
    Serial.print("Temperatura: ");
    Serial.print(t);
    Serial.println(" *C");
  }
}

void initPins() {
  pinMode(D5, OUTPUT);
  digitalWrite(D5, 0);
}

void initSerial() {
  Serial.begin(115200);
}
void initWiFi() {
  delay(10);
  Serial.println("Conectando-se em: " + String(SSID));

  WiFi.begin(SSID, PASSWORD);
  while (WiFi.status() != WL_CONNECTED) {
    delay(100);
    Serial.print(".");
  }
  Serial.println();
  Serial.print("Conectado na Rede " + String(SSID) + " | IP => ");
  Serial.println(WiFi.localIP());
}

// Funcão para se conectar ao Broker MQTT
void initMQTT() {
  MQTT.setServer(BROKER_MQTT, BROKER_PORT);
  MQTT.setCallback(mqtt_callback);
}

//Função que recebe as mensagens publicadas
void mqtt_callback(char* topic, byte* payload, unsigned int length) {

}

void reconnectMQTT() {
  while (!MQTT.connected()) {
    Serial.println("Tentando se conectar ao Broker MQTT: " + String(BROKER_MQTT));
    if (MQTT.connect(NAMECLIENTE, MQTTUSERNAME, MQTTPASSWORD)) {
      Serial.println("Conectado");
      MQTT.subscribe("/casa/lampada/sala");

    } else {
      Serial.println("Falha ao Reconectar");
      Serial.println("Tentando se reconectar em 2 segundos");
      delay(2000);
    }
  }
}

void recconectWiFi() {
  while (WiFi.status() != WL_CONNECTED) {
    delay(100);
    Serial.print(".");
  }
}
