#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <Adafruit_BMP085.h>


// CONSt WIFI
const char* SSID = "AGUIAR_ADM"; // rede wifi
const char* PASSWORD = "a0b9c8d7e6"; // senha da rede wifi

const char* BROKER_MQTT = "192.168.10.101"; // ip/host do broker
int BROKER_PORT = 1883; // porta do broker
const char* NAMECLIENTE = "ALUNO01";
const char* MQTTUSERNAME = "autohome";
const char* MQTTPASSWORD = "comida05";

const char* topicoPubTemp = "/casa/temp/01";
const char* topicoPubPres = "/casa/pres/01";
const char* topicoPubAlti = "/casa/alti/01";

Adafruit_BMP085 bmp180;

// prototypes
void initPins();
void initSerial();
void initWiFi();
void initMQTT();
void initbmp180();
void pubbmp180();



WiFiClient espClient;
PubSubClient MQTT(espClient); // instancia o mqtt

// setup
void setup() {
//  initPins();
  initSerial();
  initWiFi();
  initMQTT();
  initbmp180();
}

void loop() {
  if (!MQTT.connected()) {
    reconnectMQTT();
  }
  recconectWiFi();
  MQTT.loop();
  delay(2000);
  pubbmp180();
}

// implementacao dos prototypes


void initbmp180()
{
 if (!bmp180.begin()) 
  {
    Serial.println("Sensor nao encontrado !!");
    while (1) {}
  }
  
}

void pubbmp180()
{
    // A leitura da temperatura e umidade pode levar 250ms!
  // O atraso do sensor pode chegar a 2 segundos.
  float p = bmp180.readPressure();
  float t = bmp180.readTemperature();
  float a = bmp180.readAltitude();
  // testa se retorno é valido, caso contrário algo está errado.
  if (isnan(t) || isnan(p)) 
  {
    
    Serial.println("Failed to read from DHT");
  } 
  else
  {
    char msg[30];
    char val[10];
    p = p/101325;
    Serial.print("Pressão: ");
    Serial.print(p);
    Serial.print(" Temperatura: ");
    Serial.print(t);
    Serial.print(" Altitude: ");
    Serial.print(a);

    dtostrf(t, 3, 2, msg);
    MQTT.publish(topicoPubTemp, msg);
    dtostrf(p, 3, 2, msg);
    MQTT.publish(topicoPubPres, msg);
    dtostrf(a, 3, 2, msg);
    MQTT.publish(topicoPubAlti, msg);
  }
}

void initPins() {
  pinMode(D5, OUTPUT);
  digitalWrite(D5, 0);
}

void initSerial() {
  Serial.begin(115200);
}
void initWiFi() {
  delay(10);
  Serial.println("Conectando-se em: " + String(SSID));

  WiFi.begin(SSID, PASSWORD);
  while (WiFi.status() != WL_CONNECTED) {
    delay(100);
    Serial.print(".");
  }
  Serial.println();
  Serial.print("Conectado na Rede " + String(SSID) + " | IP => ");
  Serial.println(WiFi.localIP());
}

// Funcão para se conectar ao Broker MQTT
void initMQTT() {
  MQTT.setServer(BROKER_MQTT, BROKER_PORT);
  MQTT.setCallback(mqtt_callback);
}

//Função que recebe as mensagens publicadas
void mqtt_callback(char* topic, byte* payload, unsigned int length) {

}

void reconnectMQTT() {
  while (!MQTT.connected()) {
    Serial.println("Tentando se conectar ao Broker MQTT: " + String(BROKER_MQTT));
    if (MQTT.connect(NAMECLIENTE, MQTTUSERNAME, MQTTPASSWORD)) {
      Serial.println("Conectado");
      MQTT.subscribe("/casa/lampada/sala");

    } else {
      Serial.println("Falha ao Reconectar");
      Serial.println("Tentando se reconectar em 2 segundos");
      delay(2000);
    }
  }
}

void recconectWiFi() {
  while (WiFi.status() != WL_CONNECTED) {
    delay(100);
    Serial.print(".");
  }
}
