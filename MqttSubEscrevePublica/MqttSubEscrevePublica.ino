#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>

// CONSt WIFI
const char* SSID = "AGUIAR_ADM"; // rede wifi
const char* PASSWORD = "a0b9c8d7e6"; // senha da rede wifi

const char* BROKER_MQTT = "192.168.10.101"; // ip/host do broker
int BROKER_PORT = 1883; // porta do broker
const char* NAMECLIENTE = "ALUNO01";
const char* MQTTUSERNAME = "autohome";
const char* MQTTPASSWORD = "comida05";

const char* topicoSub = "/casa/lampada/01";
const char* topicoPub = "/casa/confirma/01";


// prototypes
void initPins();
void initSerial();
void initWiFi();
void initMQTT();

WiFiClient espClient;
PubSubClient MQTT(espClient); // instancia o mqtt

// setup
void setup() {
  initPins();
  initSerial();
  initWiFi();
  initMQTT();
}

void loop() {
  if (!MQTT.connected()) {
    reconnectMQTT();
  }
  recconectWiFi();
  MQTT.loop();
}

// implementacao dos prototypes

void initPins() {
  pinMode(D5, OUTPUT);
  digitalWrite(D5, 0);
}

void initSerial() {
  Serial.begin(115200);
}
void initWiFi() {
  delay(10);
  Serial.println("Conectando-se em: " + String(SSID));

  WiFi.begin(SSID, PASSWORD);
  while (WiFi.status() != WL_CONNECTED) {
    delay(100);
    Serial.print(".");
  }
  Serial.println();
  Serial.print("Conectado na Rede " + String(SSID) + " | IP => ");
  Serial.println(WiFi.localIP());
}

// Funcão para se conectar ao Broker MQTT
void initMQTT() {
  MQTT.setServer(BROKER_MQTT, BROKER_PORT);
  MQTT.setCallback(mqtt_callback);
}

//Função que recebe as mensagens publicadas
void mqtt_callback(char* topic, byte* payload, unsigned int length) {

  String message;
  for (int i = 0; i < length; i++) {
    char c = (char)payload[i];
    message += c;
  }
  Serial.println("Tópico => " + String(topic) + " | Valor => " + String(message));
  if (message == "1") {
    digitalWrite(D5, 1);
    Serial.println("Ligado");
    MQTT.publish(topicoPub, "On");
  } else {
    digitalWrite(D5, 0);
    Serial.println("Desligado");
    MQTT.publish(topicoPub, "Off");
  }
  Serial.flush();
}

void reconnectMQTT() {
  while (!MQTT.connected()) {
    Serial.println("Tentando se conectar ao Broker MQTT: " + String(BROKER_MQTT));
    if (MQTT.connect(NAMECLIENTE, MQTTUSERNAME, MQTTPASSWORD)) {
      Serial.println("Conectado");
      MQTT.subscribe(topicoSub);

    } else {
      Serial.println("Falha ao Reconectar");
      Serial.println("Tentando se reconectar em 2 segundos");
      delay(2000);
    }
  }
}

void recconectWiFi() {
  while (WiFi.status() != WL_CONNECTED) {
    delay(100);
    Serial.print(".");
  }
}
